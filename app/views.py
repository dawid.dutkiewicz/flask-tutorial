from flask import flash, redirect, render_template, url_for
from flask_login import current_user, login_user

from app import app, forms, models


@app.route('/')
@app.route('/index')
def index():
    user = {
        'username': 'Dave',
        'email': 'dave@gmail.com'
    }
    posts = [
        {
            'title': 'Post 1',
            'content': 'lorem ipsum'
        },
        {
            'title': 'Post 2',
            'content': 'lorem ipsum'
        },
        {
            'title': 'Post 3',
            'content': 'lorem ipsum'
        },
        {
            'title': 'Post 4',
            'content': 'lorem ipsum'
        },
    ]

    return render_template(
        'index.html',
        title='Flask tutorial',
        user=user,
        posts=posts
    )


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = forms.LoginForm()
    if form.validate_on_submit():
        user = models.User.query.filter_by(username=form.username.data).first()
        if user is None or user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))

    return render_template('login.html', form=form)
